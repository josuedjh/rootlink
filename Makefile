
default:
	pip install -r requirements-develop.txt

collectstatic:
	rm -rf public/static; \

	mkdir -p public/static; \
	mkdir -p public/media; \

	python \
        src/manage.py collectstatic \
        --noinput \
        --no-color \
        --traceback \
        --verbosity=1 \
        --ignore "*.vue" \
        --ignore "*.md" \
        --ignore ".cache" \
        --ignore "node_modules" \
        --ignore "src"

.PHONY: backend
backend:
	docker-compose run --rm --service-ports backend --shell

.PHONY: runserver
runserver:
	 python src/manage.py runserver 0.0.0.0:8000

.PHONY: build
build:
	cd src/rooflink/static && yarn build
	docker-compose run \
		--no-deps \
		--rm backend \
		gosu app make collectstatic
	docker build -t \
		registry.gitlab.com/humanzilla/rooflink-training/backend:production .
	docker push \
		registry.gitlab.com/humanzilla/rooflink-training/backend:production

.PHONY: deploy
deploy:
	heroku container:push web
	heroku container:release web
	heroku run bash -c "python src/manage.py migrate"