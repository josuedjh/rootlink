FROM registry.gitlab.com/humanzilla/rooflink-training/backend:latest

RUN set -eux; \
    gosu app python3 -m venv /python; \
    gosu app /python/bin/pip install -U pip setuptools wheel

COPY --chown=app:app requirements.txt /requirements.txt

RUN set -eux; \
    chown app.app /requirements.txt; \
    gosu app pip install --no-cache-dir -r /requirements.txt;

COPY --chown=app:app . /app

RUN set -eux; \
    \
    export DJANGO_SETTINGS_MODULE=rooflink.settings.production; \
    \
# Collect static files
    \
    gosu app mkdir -p public/assets; \
    gosu app mkdir -p public/media; \
    gosu app python \
        src/manage.py collectstatic \
        --noinput \
        --no-color \
        --traceback \
		--verbosity 0 \
        --ignore ".cache" \
        --ignore "node_modules" \
        --ignore "src" \
        --clear

CMD uwsgi --ini uwsgi.ini