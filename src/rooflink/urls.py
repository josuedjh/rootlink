import pdb

from django.conf import settings
from django.contrib import admin
from django.shortcuts import render
from django.urls import path
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from wagtail.contrib.sitemaps.views import sitemap

from wagtail.core import urls as wagtail_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls


def robots_view(request):
    return render(
        request,
        [f"{request.site.root_page.slug}/robots.txt", "robots.txt"],
        content_type="text/plain",
    )


urlpatterns = [
    url(r"^sitemap\.xml$", sitemap),
    url(r"^robots\.txt$", robots_view),
    path("admin/", admin.site.urls),
    path("manager/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("", include(wagtail_urls)),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
