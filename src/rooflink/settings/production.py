from .base import *


DEBUG = False

DATABASES = {
    "default": dj_database_url.parse(
        url=os.environ.get(
            "DATABASE_URL", "postgres://postgres@database/rooflink_master"
        ),
        engine="django.db.backends.postgresql_psycopg2",
        conn_max_age=600,
        ssl_require=True,
    )
}
