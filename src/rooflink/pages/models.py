from django.contrib.auth import get_user_model
from django.db import models
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from rooflink.pages.blocks import DefaultStreamBlock
from taggit.models import TaggedItemBase
from wagtail.admin.edit_handlers import (
    FieldPanel)
from wagtail.core.fields import StreamField, RichTextField
from wagtail.core.models import Page

User = get_user_model()

Page.subpage_types = ["pages.IndexPage"]


class IndexPage(Page):
    parent_page_types = ["wagtailcore.Page"]
    subpage_types = ["SimplePage", "SectionPage"]
    content = StreamField(DefaultStreamBlock(required=False), blank=True)


class SimplePage(Page):
    parent_page_types = ["IndexPage", "SimplePage"]
    subpage_types = []
    content = StreamField(DefaultStreamBlock(required=False), blank=True)


class SectionPage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]

    parent_page_types = ["IndexPage", "SimplePage"]
    subpage_types = ["ArticlePage"]


class TagPages(TaggedItemBase):
    content_object = ParentalKey(
        'ArticlePage',
        related_name='article_tags',
        on_delete=models.CASCADE
    )


class ArticlePage(Page):
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=TagPages, blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body'),
        FieldPanel('tags'),
    ]

    parent_page_types = ["SectionPage"]
    subpage_types = []

    def section_page(self):
        return Page.objects.type(SectionPage)